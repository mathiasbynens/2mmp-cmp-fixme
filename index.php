<?php

// bereken de oppervlakte van een rechthoek op basis van de breedte en de hoogte
function berekenOppervlakteRechthoek($breedte, $hoogte) {
	// http://i.imgur.com/BZXVL5j.png
	return (int) ($breedte + 0.5 * $hoogte * pi());
}

$isPost = false;
$breedte = 4; // standaardwaarde
$hoogte = 8; // standaardwaarde
if (isset($_POST) && isset($_POST['breedte']) && isset($_POST['hoogte'])) {
	$isPost = true;
	$breedte = (int) $_POST['breedte'];
	$hoogte = (int) $_POST['hoogte'];
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Bereken de oppervlakte van een rechthoek</title>
		<style>
			html, input { font: 1em/1.5 sans-serif; }
			.calculation-result { padding: 5px; background: lightgreen; border: 3px double green; }
		</style>
	</head>
	<body>
		<h1>Bereken de oppervlakte van een rechthoek</h1>
		<form method="post" action="index.php">
			<div><label>Breedte (cm): <input type="number" min="0" max="50" step="1" value="<?php echo $breedte; ?>" name="breedte" autofocus></label></div>
			<div><label>Hoogte (cm): <input type="number" min="0" max="50" step="1" value="<?php echo $hoogte; ?>" name="hoogte"></label></div>
			<div><input type="submit" value="Bereken"></div>
		</form>
<?php
if ($isPost) {
?>
<p id="resultaat">Een rechthoek met een breedte van <strong><?php echo $breedte; ?> cm</strong> en een hoogte van <strong><?php echo $hoogte; ?> cm</strong> heeft een oppervlakte van <strong class="calculation-result"><?php echo berekenOppervlakteRechthoek($breedte, $hoogte); ?> cm</strong>. Op schaal ziet deze rechthoek er zo uit:</p>
<div style="width: <?php echo $breedte * 50; ?>px; height: <?php echo $hoogte * 50; ?>px; background: #bada55;"></div>
<?php
} // end if
?>
	</body>
</html>